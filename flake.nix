{
  description = "FireDragon's documentation flake ❄️";

  nixConfig = {
    extra-substituters = ["https://devenv.cachix.org"];
    extra-trusted-public-keys = ["devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw="];
  };

  inputs = {
    devenv = {
      url = "github:cachix/devenv";
      inputs.flake-compat.follows = "flake-compat";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.pre-commit-hooks.follows = "pre-commit-hooks";
    };
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
    };
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.flake-compat.follows = "flake-compat";
      inputs.nixpkgs-stable.follows = "nixpkgs";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    flake-parts,
    ...
  } @ inp: let
    inputs = inp;
    perSystem = {pkgs, ...}: {
      devenv.shells.default = {
        enterShell = ''
          echo "Entering FireDragon's documentation devenv!"
          echo ""
          echo "The following actions are available:"
          echo "- mdbook serve: Serve the documentation locally"
          echo "- mdbook build: Build the documentation"
          echo "- pre-commit run --all-files: Run all pre-commit hooks"
        '';
        name = "FireDragon documentation";
        pre-commit.hooks = {
          commitizen.enable = true;
          flake-checker.enable = true;
          markdownlint.enable = true;
          typos.enable = true;
        };
        packages = with pkgs; [
          commitizen
          markdownlint-cli
          mdbook
          mdbook-admonish
          mdbook-emojicodes
          typos
        ];
      };
    };
  in
    flake-parts.lib.mkFlake {inherit inputs;} {
      # Imports flake-modules
      imports = [
        inputs.devenv.flakeModule
        inputs.pre-commit-hooks.flakeModule
      ];

      # The systems currently available
      systems = ["x86_64-linux" "aarch64-linux"];

      # This applies to all systems
      inherit perSystem;
    };
}
