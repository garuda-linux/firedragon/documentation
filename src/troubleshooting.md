# Troubleshooting steps

If you have some issues with Firedragon, follow these steps, remember each result and if the problem persists please post all your results along with the issue.

- Close the browser before trying anything else
- Create a new profile by opening the terminal and typing
~~~
firedragon -P
~~~
- Proceed by creating the new profile and launching from it to test your issue
- If the issue persists, rename `/usr/lib/firedragon/firedragon.cfg` and `~/.firedragon/[YOUR_PROFILE]/firedragon.overrides.cfg` to something else and relaunch Firedragon (Note: You obviously have to replace "[YOUR_PROFILE]" by the exact name of your profile in the above command)
  - The UI will look quite different (if not, you got a profile corruption) then test your issue
  - If the issue is gone, here are some tips on how to find the setting in `firedragon.cfg` that causes it:
    - Instead of changing every setting one by one, delete an entire category or section. Then relaunch.
      If nothing changes, bring back the settings and perform the same action on the next category/section.
      Once it does change something, then you know for sure it’s one or multiple settings part of that Category/Settings.
      This method will save you huge amount of time.
- Change your _User Agent_ to something different (go in Section **USER AGENT** from firedragon.cfg) and validate it did change going on [whatismybrowser.com](https://www.whatismybrowser.com/) before you test your issue

If after all these steps you still get the same issue, then it is very most likely not Firedragon related and you should open a help Request on the [Garuda Forum](https://forum.garudalinux.org) or the [Floorp github](https://github.com/Floorp-Projects/Floorp).

If you stop getting the issue at one of those steps, please provide details on the [Garuda Forum](https://forum.garudalinux.org), so we might be able to fix for everyone.

Firedragon is in essence “not a browser”. In reality, it is a few patches to the source code combined with some config files (the most important being `/usr/lib/firedragon/firedragon.cfg`).
The very most of what makes Firedragon "Firedragon" is found in this file. This is why testing without the file at all is a good indication of next steps.
