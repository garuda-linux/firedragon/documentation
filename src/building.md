# Building FireDragon

Building FireDragon yourself for already known targets is quite easy using our build system.

## Obtaining it

First, clone the repository:

```sh
git clone https://gitlab.com/garuda-linux/firedragon/builder.git
cd builder
```

Then, proceed by running one of the following commands. You will need a few commands available for this to succeed, such as:

- `curl`
- `git`
- `make`
- `rsync`
- `tar`
- `zstd`

## Currently implemented targets

### Source archive

```sh
make source
```

This downloads the Floorp source (if required), applies all patches, copies additional source files and installs FireDragon settings.
Output source archive: `build/firedragon-vX.X.X-X.source.tar.zst`.

### Linux x86_64 archive

```sh
make linux-x86_64
```

This creates the source files for FireDragon (if required) and builds it for linux x86_64 architecture.
Output linux x86_64 archive: `build/firedragon-vX.X.X-X.linux-x86_64.tar.bz2`.

### AppImage x86_64

```sh
make appimage-x86_64
```

This uses the Linux x86_64 archive (building it if required) to create the FireDragon AppImage for x86_64 architecture.
Output AppImage x86_64: `build/firedragon-vX.X.X-X.appimage-x86_64.AppImage`.

## Further targets

If you wish to make FireDragon available for another distribution or target, absolutely feel free to. We are happy for every help :smile:
