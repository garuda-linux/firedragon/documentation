# FireDragon

[![pipeline status](https://gitlab.com/garuda-linux/firedragon/builder/badges/main/pipeline.svg)](https://gitlab.com/garuda-linux/firedragon/builder/-/commits/main)
[![Latest Release](https://gitlab.com/garuda-linux/firedragon/builder/-/badges/release.svg)](https://gitlab.com/garuda-linux/firedragon/builder/-/releases)

Welcome to the FireDragon documentation / landing page! :dragon:
In here, we collect common information about the browser, as well as known workarounds and troubleshooting stepos.

## What is this?

FireDragon is a browser based on the excellent [Floorp browser](https://floorp.app/en) (also called the Vivaldi of Firefox's).
It was customized to have [dr460nized](https://garudalinux.org/images/garuda/download/dr460nized/garuda-dr460nized.webp)-fitting aesthetics as well as many opiniated settings by default.
As this browser was originally a [Librewolf](https://librewolf.org) fork, we are trying to integrate its best patches and tweaks in the new base.
The most important features in addition of Floorps own ones can be found below.

- [Searx](https://searx.garudalinux.org/) & [Whoogle](https://search.garudalinux.org/) search engines added, with the possibility to run locally if fitting deps are installed
- The default search engine is Garuda's [SearxNG instance](https://searx.garudalinux.org)
- [Dark Reader](https://addons.mozilla.org/en-US/firefox/addon/darkreader/)
- [Sweet theme](https://github.com/EliverLara/firefox-sweet-theme) added
- Custom, dr460nized branding :dragon:
- Keeps privacy-enhancing settings in sync with [Librewolfs](https://librewolf.org) changes and some custom ones
- Firefox accounts are enabled and profile data is synced to a custom self-hosted sync server (`ffsync.garudalinux.org`)
- Presets for both `profile-sync-daemon` (which Garuda Linux ships by default) & `Firejail` are available
- Faster webpages loading from:
  - Custom Firedragon settings
  - Inclusion of [FastFox tweaks](https://github.com/yokoffing/Betterfox/blob/main/README.md)
  - Disabling Media Autoplay
- Hidden Navigation buttons instead of being greyed out when they are inactive
- [PBMode Security](https://wiki.mozilla.org/Security/Tracking_protection)
- Latest Fingerprinting as an option in `firedragon.cfg` (you can copy to your own `firedragon.overrides.cfg` and enable there)

<img src="https://gitlab.com/garuda-linux/firedragon/settings/-/raw/master/home.png" alt="FireDragon Screenshot">

## Where to get it?

There are multiple ways to obtain FireDragon on Linux. Windows builds are not being provided, however.

- Archlinux builds can be obtained in a few ways:
  - Install it via the `firedragon` [AUR package](https://aur.archlinux.org/packages/firedragon)
  - Install it via the `firedragon-bin` [AUR package](https://aur.archlinux.org/packages/firedragon-bin) if you do not wish to compile the package yourself
  - Or use the [Chaotic-AUR](https://aur.chaotic.cx) repository to install and update it with Pacman (maintained by the FireDragon maintainer [dr460nf1r3](https://gitlab.com/dr460nf1r3))
- NixOS builds available at [Chaotic-Nyx](https://nyx.chaotic.cx)
  - Either install the flake and put `firedragon` as any other Nixpkgs package
  - Or run it directly via `nix run github:chaotic-cx/nyx/nyxpkgs-unstable#firedragon`
- Flatpak builds can be fetched from [FlatHub](https://flathub.org/apps/org.garudalinux.firedragon)
- AppImage builds are available via our [GitLab releases](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/permalink/latest)
- Binary tarballs can also be obtained via [GitLab releases](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/permalink/latest)
