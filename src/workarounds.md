# Workarounds and Known Issues

## Issues on twitch.tv
See this issue for reference: [click me](https://codeberg.org/librewolf/issues/issues/646#issuecomment-1716563)

## Add-ons and Settings menus broken
See https://forum.garudalinux.org/t/firedragon-broken-after-117-update/38762
Possible fix is to set to **false** the config `widget.wayland.fractional-scale.enabled` in *about:config*

## Firefox Translate feature not working
Firefox offers a Translate page feature that by default does not work with Firedragon.

This situation was brought up by [this](https://forum.garudalinux.org/t/firedragon-translate-not-working-fix/39529 ) post on the Garuda Forum.

It is due to the following setting in **/usr/lib/firedragon/firedragon.cfg**: `defaultPref("services.settings.server", "https://%.invalid")`

After revision, according to [this](https://groups.google.com/g/mozilla.support.firefox/c/O2Rswb1jBUw?pli=1) it seems to be the server where data is transmitted to Mozilla, not only for the Translate feature. Since Firedragon aims at privacy, we disabled it.

You can enable it by setting a proper server (up to you to find one) and adding the config in your user `firedragon.overrides.cfg`.

## TABs loaded in Background
This is not an issue, but by design. By default Firedragon loads tabs in the background. Some people may not like this, therefore you can change the setting `browser.tabs.loadDivertedInBackground` to **FALSE**.
