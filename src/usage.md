# How to understand and use Firedragon Settings

Firedragon is, apart from a few exceptions, a huge set of Firefox config keys.

All configurations that set Firedragon apart from Floorp are found in one file: `/usr/lib/firedragon/firedragon.cfg`.

The file is seperated in Categories and Sections, in which settings are classified. This is not a perfect 1-1 match but most of the time it makes sense to identify.

To understand what each setting means, you have to copy the setting key and search for it in a Search Engine. This sometimes leads to instant description, sometimes it's much harder, especially when
it's a setting Mozilla did not document.

When you want to change those settings, or add new ones, the best way to do so, at a USER (not root/system!) level, is to use the file _firedragon.overrides.cfg_.
You can create that file in your user's PROFILE of Firedragon, located in `~/.firedragon/`.
And then you follow the same way of setting keys as in _firedragon.cfg_.
The overrides file will override **only** those you specificy in it, it will **NOT** replace the entire _firedragon.cfg_.

Garuda encourages users to play with those settings, so everyone can customize Firedragon to their liking. If Firefox allows something to be customized, Firedragon should as well.

Have fun!
