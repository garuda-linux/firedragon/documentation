# Credits

Creating this browser woudln't have been possible without those people and projects:

- [Mozilla](https://www.mozilla.org) (creators of the original Firefox browser)
- [Arch Linux](https://archlinux.org/) (spent a lot of time creating PKGBUILDs this browser came from early on)
- The [Floorp project](https://floorp.app/en), used as base for this browser
- The [Librewolf](https://librewolf-community.gitlab.io/) project, where a lot of the privacy enhancing tweaks originate from
- torvic9 & his retired [Plasmafox](https://github.com/torvic9/plasmafox), has also been an inspiration early on
- [vnepogodin](https://aur.archlinux.org/account/vnepogodin), who helped debugging issues and provided a lot of useful information
- [ptr1337](https://aur.archlinux.org/account/ptr1337), who also contributed a lot of helpful fixes and information and now works on Cachy Browser with vnepogodin
- [SGS](https://gitlab.com/SGSm) & [zoeronen](https://gitlab.com/zoeronen), who both contributed beautiful artwork to FireDragon
