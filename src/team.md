# Team

## Who are we?

This is the team of awesome people who are creating FireDragon for you:

- [FGD](https://gitlab.com/FGD-Garuda)
- [dr460nf1r3](https://gitlab.com/dr460nf1r3)
- [stefanwimmer128](https://gitlab.com/stefanwimmer128)

## How to contact us?

If you want to contact us due to any reason, be it suggestions or bug reports, please the issues section of the [settings](https://gitlab.com/garuda-linux/firedragon/settings) repository on GitLab.

You may also create a thread in the Garuda forum, we have a [dedicated section](https://forum.garudalinux.org/c/issues-assistance/firedragon-browser-support/47) for FireDragon issues.
Team members are present here as well.
