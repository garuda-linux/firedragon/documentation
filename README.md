# FireDragon landing page / documentation

Contains the source for the website found on [firedragon.garudalinux.org](https://firedragon.garudalinux.org).

## How to work with this repository

You need the following apps:

- [mdbook](https://rust-lang.github.io/mdBook/index.html)
- [mdbook-admonish](https://github.com/tommilligan/mdbook-admonish) (on Arch available via [AUR](https://aur.archlinux.org/packages/mdbook-admonish))
- [mdbook-emojicodes](https://github.com/blyxyas/mdbook-emojicodes)

Then, a development server can be started by running `mdbook serve` in the root directory of this repository.

## General handling

The entrypoint for all documentation files is `src/intro.md`. Any pages should be linked in `src/SUMMARY.md`.
Generally, one can use any Markdown syntax and more thanks to mdBook features. The most important sections are linked below:

- [SUMMARY.md](https://rust-lang.github.io/mdBook/format/summary.html)
- [mdBook-specific features](https://rust-lang.github.io/mdBook/format/mdbook.html)
- [Markdown syntax](https://rust-lang.github.io/mdBook/format/markdown.html)

Additionally, [mdbook-emojicodes](https://github.com/blyxyas/mdbook-emojicodes) will convert any emojicodes such as `:dragon:` to a real emoji.
Meanwhile, [mdbook-admonish](https://github.com/tommilligan/mdbook-admonish) renders additional blocks to styled banners (see it in action [here](https://tommilligan.github.io/mdbook-admonish/).

## Checking changes for typos

Install the `typos` package and run `typos` inside the main directory of this repository.
Then correct any typos or add unknown words to the `_typos.toml` file.

## Deploying changes

Deploying changes is as easy as pushing the changes to the main branch.
After successful typos/commitizen checks, the book will be built and the artifact gets pushed to the cf-pages branch.
This triggers an automated deployment to Cloudflare pages.

# Development tools

## Nix flake

There is a `flake.nix` with all required tools available. Enter it with `nix develop --impure`.
